package com.example.anton.mygame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


public class MainActivity extends AppCompatActivity {

    Button [] B;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        B=new Button[16];
        B[0]= findViewById(R.id.button1);
        B[1]= findViewById(R.id.button2);
        B[2]= findViewById(R.id.button3);
        B[3]= findViewById(R.id.button4);
        B[4]= findViewById(R.id.button5);
        B[5]= findViewById(R.id.button6);
        B[6]= findViewById(R.id.button7);
        B[7]= findViewById(R.id.button8);
        B[8]= findViewById(R.id.button9);
        B[9]= findViewById(R.id.button10);
        B[10]= findViewById(R.id.button11);
        B[11]= findViewById(R.id.button12);
        B[12]= findViewById(R.id.button13);
        B[13]= findViewById(R.id.button14);
        B[14]= findViewById(R.id.button15);
        B[15]= findViewById(R.id.button16);

    }

    /**
     * Метод для перемещения кнопки.
     * @param B1 - первыя кнопка
     * @param B2 - вторая кнопка
     */
    public void swap(Button B1, Button B2)
    {
        if (B2.getVisibility() == View.INVISIBLE)
        {
            B2.setVisibility(View.VISIBLE);
            B1.setVisibility(View.INVISIBLE);
            String Tmp = B1.getText().toString();
            B1.setText(B2.getText());
            B2.setText(Tmp);
        }
    }

    /**
     * Метод для определения нажатия.
     */
    public void buttonOnClick(View V) {

        Button current = (Button)V;

        String N = current.getTag().toString();

        int n = Integer.parseInt(N)-1;

        int y = n / 4;

        int x = n;
        if (n >= 12) x = n - 12;
        else
        if (n >= 8) x = n - 8;
        else
        if (n >= 4) x = n - 4;

        int nc = y * 4 + x;
        int nt = (y - 1) * 4 + x;
        int nb = (y + 1) * 4 + x;
        int nl= y * 4 + x-1;
        int nr = y * 4 + x + 1;

        if (y - 1 >= 0)
            swap(B[nc], B[nt]);
        if (y + 1 < 4)
            swap(B[nc], B[nb]);
        if (x - 1 >= 0)
            swap(B[nc], B[nl]);
        if (x + 1 < 4)
            swap(B[nc], B[nr]);
    }
}